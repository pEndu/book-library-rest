package com.example.booklibrary.controller;

import com.example.booklibrary.domain.Author;
import com.example.booklibrary.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/authors")
public class AuthorController {

    private AuthorService authorService;

    @Autowired
    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public Iterable<Author> list() {
        return authorService.list();
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public Author create(@RequestBody Author author) {
        return authorService.create(author);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Author read(@PathVariable(value = "id") long id) {
        return authorService.read(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public Author update(@PathVariable(value = "id") long id, @RequestBody Author author) {
        return authorService.update(id, author);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable(value = "id") long id) {
        authorService.delete(id);
    }
}