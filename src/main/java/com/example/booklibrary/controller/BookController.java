package com.example.booklibrary.controller;

import com.example.booklibrary.domain.Book;
import com.example.booklibrary.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/books")
public class BookController {

    private BookService bookService;

    @Autowired
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public Iterable<Book> list() {
        return bookService.list();
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public Book create(@RequestBody Book book) {
        return bookService.create(book);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Book read(@PathVariable(value = "id") long id) {
        return bookService.read(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public Book update(@PathVariable(value = "id") long id, @RequestBody Book book) {
        return bookService.update(id, book);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable(value = "id") long id) {
        bookService.delete(id);
    }
}