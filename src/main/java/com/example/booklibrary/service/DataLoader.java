package com.example.booklibrary.service;

import com.example.booklibrary.domain.Author;
import com.example.booklibrary.domain.Book;
import com.example.booklibrary.repository.AuthorRepository;
import com.example.booklibrary.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class DataLoader {
    private BookRepository bookRepository;
    private AuthorRepository authorRepository;

    @Autowired
    public DataLoader(BookRepository bookRepository, AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
    }

    @PostConstruct
    private void loadData() {
        Author author = new Author("Michal", "Wojcik", "Poland");
        authorRepository.save(author);

        Author author2 = new Author("John", "Doe", "USA");
        authorRepository.save(author2);

        Book book = new Book("Spring REST appliation");
        book.setAuthor(author);
        book.setIsbn("48651685681");
        book.setPublisher("Softylabs");
        book.setYearPublished(2018);

        bookRepository.save(book);

        Book book2 = new Book("Praca inzynierska");
        book2.setAuthor(author);
        book2.setIsbn("5745416586");
        book2.setPublisher("Politechnika Koszalinska");
        book2.setYearPublished(2017);

        bookRepository.save(book2);

        Book book3 = new Book("Random book");
        book3.setAuthor(author2);
        book3.setIsbn("8961518618");
        book3.setPublisher("Random publisher");
        book3.setYearPublished(2018);

        bookRepository.save(book3);
    }
}
