package com.example.booklibrary.service;

import com.example.booklibrary.domain.Author;

public interface AuthorService {

    Iterable<Author> list();

    Author create(Author author);

    Author read(long id);

    Author update(long id, Author author);

    void delete(long id);
}
