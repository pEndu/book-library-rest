package com.example.booklibrary.service;

import com.example.booklibrary.domain.Book;
import com.example.booklibrary.repository.AuthorRepository;
import com.example.booklibrary.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import static com.example.booklibrary.Utils.Utils.isNotNull;

@Service
public class BookServiceImpl implements BookService {

    private BookRepository bookRepository;
    private AuthorRepository authorRepository;

    @Autowired
    public BookServiceImpl(BookRepository bookRepository, AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
    }

    @Override
    public Iterable<Book> list() {
        return bookRepository.findAll(new Sort(Sort.Direction.DESC, "yearPublished"));
    }

    @Transactional
    @Override
    public Book create(Book book) {
        authorRepository.save(book.getAuthor());
        return bookRepository.save(book);
    }

    @Override
    public Book read(long id) {
        return bookRepository.findOne(id);
    }

    @Override
    public Book update(long id, Book update) {
        Book book = bookRepository.findOne(id);
        if (isNotNull(update.getTitle()))
            book.setTitle(update.getTitle());
        return bookRepository.save(book);
    }

    @Transactional
    @Override
    public void delete(long id) {
        bookRepository.deleteBookById(id);
    }

}
