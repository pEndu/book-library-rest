package com.example.booklibrary.service;

import com.example.booklibrary.domain.Book;

public interface BookService {

    Iterable<Book> list();

    Book create(Book book);

    Book read(long id);

    Book update(long id, Book book);

    void delete(long id);
}
