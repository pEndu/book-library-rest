package com.example.booklibrary.service;

import com.example.booklibrary.domain.Author;
import com.example.booklibrary.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import static com.example.booklibrary.Utils.Utils.isNotNull;

@Service
public class AuthorServiceImpl implements AuthorService {

    private AuthorRepository authorRepository;

    @Autowired
    public AuthorServiceImpl(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Override
    public Iterable<Author> list() {
        return authorRepository.findAllByOrderByLastName();
    }

    @Transactional
    @Override
    public Author create(Author author) {
        return authorRepository.save(author);
    }

    @Override
    public Author read(long id) {
        return authorRepository.findOne(id);
    }

    @Override
    public Author update(long id, Author update) {
        Author author = authorRepository.findOne(id);
        if (isNotNull(update.getFirstName()))
            author.setFirstName(update.getFirstName());
        if (isNotNull(update.getLastName()))
            author.setLastName(update.getLastName());
        if (isNotNull(author.getCountry()))
            author.setCountry(update.getCountry());
        return authorRepository.save(author);
    }

    @Override
    public void delete(long id) {
        authorRepository.delete(id);
    }

}
