package com.example.booklibrary.repository;

import com.example.booklibrary.domain.Author;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuthorRepository extends CrudRepository<Author, Long> {

    List<Author> findAllByOrderByLastName();
}
