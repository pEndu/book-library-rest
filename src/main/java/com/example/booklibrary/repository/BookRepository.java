package com.example.booklibrary.repository;

import com.example.booklibrary.domain.Book;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {

    List<Book> findAll(Sort sort);

    @Modifying
    @Query("DELETE FROM Book b WHERE b.id = :id")
    void deleteBookById(@Param("id") Long id);
}
